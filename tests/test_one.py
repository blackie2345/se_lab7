import pytest
from ..src.case_handlers import *

def get_path (filename):
    return "/home/osirv/pi_labs/markov/se_lab7/tests/test_assets/" + filename
    

def test_case_file_exsist_true():
    """ testiraj za putanja koja postoji"""
    path= get_path("file.html")
    handler=CaseFileExistsHandler()
    assert handler.test(path) == True


def test_case_file_exsist_false ():
    path= get_path("filea.html")
    handler = CaseFileExistsHandler()
    assert handler.test (path)== False



def test_case_file_exists_true_run ():
    path= get_path ("file.html")
    handler= CaseFileExistsHandler()
    assert "This file exists" in handler.run(path)


def test_case_file_exists_false_run ():
    path = get_path ("filea.html")
    handler = CaseFileExistsHandler()
    with pytest.raises(CaseError) as error:
        handler.run(path)
    assert error.value.error_code ==500


def test_case_file_not_exsist_true():
    path= get_path("filea.html")
    handler=CaseFileNotExistsHandler()
    assert handler.test(path) == True


def test_case_file_notexsist_false ():
    path= get_path("filea.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(path)== True
